# Readme

This ansible role installs an configures bareos-filedaemon from the bareos repository.

It will also copy an empty `prebackup.sh` script to `/usr/local/sbin/prebackup.sh` you can add your own `prebackup.sh` by adding one to `files/prebackup.sh` in your ansible directory.

## Required vars

```ansible
bareos_encryption_master_public_key: "bareos_master_public.pem"
bareos_encryption_host_keypair: "bareos_host_keypair.pem"

bareos_director_domain: "your-director-fqdn"
bareos_director_password: "your-director-password"
bareos_client_domain: "your-client-fqdn"
bareos_client_console_password: "your-console-password"
```

## Optional vars with defaults

```ansible
# bareos version number
bareos_version: "20"

# path to bareos package repo
bareos_apt_repo: "https://download.bareos.org/bareos/release/{{bareos_version}}/Debian_10"

# path where encryption keys can be found
bareos_encryption_key_source_path: "environments/{{ env }}/group_vars/credentials/"

# name of bareos director
bareos_director_name: "{{ bareos_director_domain }}-dir"

# maximum cuncurrent jobs the client will execute
bareos_client_max_concurrent_jobs: 1

# maximum bandwidth that bareos client will consume per second
bareos_client_max_bandwidth: "40m"
```

## Role Testing with molecule

```bash
molecule test
```
